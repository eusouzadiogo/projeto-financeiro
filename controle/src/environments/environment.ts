// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCWNm0BK7_4codgm00qW_fx49tXIP24rTg',
    authDomain: 'controle-ifsp-guarulhos.firebaseapp.com',
    projectId: 'controle-ifsp-guarulhos',
    storageBucket: 'controle-ifsp-guarulhos.appspot.com',
    messagingSenderId: '29464086231',
    appId: '1:29464086231:web:47e2411e5787f43b6b1292',
    measurementId: 'G-2ZR3JJRQ8P'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
